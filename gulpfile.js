var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var ts          = require('gulp-typescript');
var tsProject   = ts.createProject("tsconfig.json");
var uglifycss   = require('gulp-uglifycss');
var uglifyjs    = require('gulp-uglify');


// HTML
// HTML into /dist
gulp.task('html', function () {
  return gulp.src('src/*.html')
    .pipe(gulp.dest("dist"))
});
// Assets into /dist
gulp.task('assets', function () {
  return gulp.src('src/assets/*')
    .pipe(gulp.dest("dist/assets"))
});

// SCSS/CSS
// SCSS to CSS into /dist & auto-inject into browsers
gulp.task('sass', function () {
  return gulp.src('src/scss/*.scss')
    .pipe(sass())
    .pipe(gulp.dest("dist/css"))
    .pipe(browserSync.stream());
});
// Uglify CSS
gulp.task('uglycss', function(){
  return gulp.src('dist/css/*.css')
    .pipe(uglifycss({
      "uglyComments": true
    }))
    .pipe(gulp.dest("dist/css"))
});
// normalize.css into /dist/js
gulp.task('normalize_css', function () {
  return gulp.src('node_modules/normalize.css/normalize.css')
    .pipe(gulp.dest("dist/css"))
});

// JS
// TS to JS into /dist/js
gulp.task("ts", function () {
  return tsProject.src()
    .pipe(tsProject())
    .js.pipe(gulp.dest("dist/js/"))
});
// Uglify JS
gulp.task('uglyjs', function (cb) {
  return gulp.src('dist/js/*.js')
    .pipe(uglifyjs())
    .pipe(gulp.dest('dist/js'))
});

// Static Server + watching scss/html files
gulp.task('default', ['dist'], function () {

  gulp.watch(['src/scss/*.scss'], ['sass']);
  gulp.watch(['src/ts/*.ts'], ['ts']);
});
gulp.task('dist', ['sass', 'ts', 'html', 'assets', 'normalize_css']);
gulp.task('ugly', ['uglycss', 'uglyjs','normalize_css'])
